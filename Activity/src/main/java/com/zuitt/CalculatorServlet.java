package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class CalculatorServlet extends HttpServlet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8405435963527493895L;
	public void init()throws ServletException{
		System.out.println("****************************");
		System.out.println("Initialize connection to database.");
		System.out.println("****************************");
	}
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException{
		double num1 = Double.parseDouble(req.getParameter("num1"));
		double num2 = Double.parseDouble(req.getParameter("num2"));
		String sym = req.getParameter("symbol");
		
		  if (sym.equals("add") || sym.equals("+")) {
			  double total = num1+num2;
			  PrintWriter out = res.getWriter();
			  out.print("<a> The two number you provide are:"+(int)num1+", "+(int)num2+"<a><br>");
				out.print("<a> The operation that you wanted is: "+sym+"<a><br>");
				out.print("<a> The result is: "+(int)total+"<a>");
		  }
		  if (sym.equals("subtract")|| sym.equals("-")) {
			  double total = num1-num2;
			  PrintWriter out = res.getWriter();
			    out.print("<a> The two number you provide are:"+(int)num1+", "+(int)num2+"<a><br>");
				out.print("<a> The operation that you wanted is: "+sym+"<a><br>");
				out.print("<a> The result is: "+(int)total+"<a>");
			  
		  }
		  if (sym.equals("multiply")|| sym.equals("*")) {
			  double total = num1*num2;
			  PrintWriter out = res.getWriter();
			  out.print("<a> The two number you provide are:"+(int)num1+", "+(int)num2+"<a><br>");
				out.print("<a> The operation that you wanted is: "+sym+"<a><br>");
				out.print("<a> The result is: "+(int)total+"<a>");
			  
		  }
		  if (sym.equals("divide")|| sym.equals("/")){
			  double total = num1/num2;
			  PrintWriter out = res.getWriter();
				out.print("<a> The two number you provide are:"+(int)num1+", "+(int)num2+"<a><br>");
				out.print("<a> The operation that you wanted is: "+sym+"<a><br>");
				out.print("<a> The result is: "+total+"<a>");
		  }
//		  else {
//			  PrintWriter out = res.getWriter();
//				out.print("No operation available");
//		  }
		  
	}
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		PrintWriter out = res.getWriter();
		out.print("<h1>You are using the Calculator App.</h1><br>");
	    out.print("To use the app, input two numbers and an operation<br>");
		out.print("hit the submit button after filling in the details<br>");
    	out.print("You will get the result to you browser!");
	}
	public void destroy() {
		System.out.println("***********************************");
		System.out.println("Disconnected from database");
		System.out.println("***********************************");
	}
}
